#ifndef TERAKI_DEV_UTILS_CRTP_HPP
#define TERAKI_DEV_UTILS_CRTP_HPP

namespace teraki {
namespace dev {

template<typename DerivedType>
struct CRTP {
  using Derived = DerivedType;
  Derived &derived() { return static_cast<Derived &>(*this); }
  Derived const &derived() const { return static_cast<Derived const &>(*this); }
};

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_CRTP_HPP */

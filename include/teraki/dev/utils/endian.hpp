#ifndef TERAKI_DEV_UTILS_ENDIAN_HPP
#define TERAKI_DEV_UTILS_ENDIAN_HPP

#include <cstdint>

namespace teraki {
namespace dev {

struct Endian {
 private:
  static constexpr uint16_t uint16_ = 0x0102;
  static constexpr uint8_t magic_ = (const uint8_t &) uint16_;
 public:
  static constexpr bool little = magic_ == 0x02;
  static constexpr bool big = magic_ == 0x01;
  static_assert(little || big, "Cannot determine endianness!");
  Endian() = delete;
};

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_ENDIAN_HPP */

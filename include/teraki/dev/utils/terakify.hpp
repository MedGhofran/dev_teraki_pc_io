#define CONCAT(X, Y) CONCAT_I(X,Y)
#define CONCAT_I(X, Y) X##Y
#define FIRST(X, ...) X
#define TAIL(X, ...) __VA_ARGS__

#define VARCOUNT(...) VARCOUNT_I(__VA_ARGS__,9,8,7,6,5,4,3,2,1,)
#define VARCOUNT_I(_, _9, _8, _7, _6, _5, _4, _3, _2, X_, ...) X_

#define INSTANTIATE(METHOD, CLASS, ...) CONCAT(INSTANTIATE_,VARCOUNT(__VA_ARGS__))(METHOD, CLASS, VARCOUNT(__VA_ARGS__), __VA_ARGS__)
#define INSTANTIATE_1(METHOD, CLASS, NUM_FIELDS, ...) METHOD(CLASS, NUM_FIELDS-VARCOUNT(__VA_ARGS__), __VA_ARGS__)
#define INSTANTIATE_2(METHOD, CLASS, NUM_FIELDS, ...) METHOD(CLASS, NUM_FIELDS-VARCOUNT(__VA_ARGS__), FIRST(__VA_ARGS__)) INSTANTIATE_1(METHOD, CLASS, NUM_FIELDS, TAIL(__VA_ARGS__))
#define INSTANTIATE_3(METHOD, CLASS, NUM_FIELDS, ...) METHOD(CLASS, NUM_FIELDS-VARCOUNT(__VA_ARGS__), FIRST(__VA_ARGS__)) INSTANTIATE_2(METHOD, CLASS, NUM_FIELDS, TAIL(__VA_ARGS__))

#define RVAL_GETTER(CLASS, INDEX, FIELD) template<> constexpr auto get<INDEX>(CLASS &&obj) { return obj.FIELD; }
#define NON_GETTER(CLASS, INDEX, FIELD) template<> constexpr auto & get<INDEX>(CLASS &obj) { return obj.FIELD; }
#define CONST_GETTER(CLASS, INDEX, FIELD) template<> constexpr const auto & get<INDEX>(const CLASS &obj) { return obj.FIELD; }
#define TUPLE_ELEMENT(CLASS, INDEX, FIELD) template<>struct tuple_element<INDEX, CLASS> {using type = std::decay_t<decltype(declval<CLASS>().FIELD)>;};

#define TERAKIFY(CLASS, ...) \
namespace std { \
  template<> struct tuple_size<CLASS> : std::integral_constant<size_t, VARCOUNT(__VA_ARGS__)> {}; \
  INSTANTIATE(TUPLE_ELEMENT, CLASS, __VA_ARGS__)                    \
  template<size_t I> constexpr auto get(CLASS &&obj);               \
  INSTANTIATE(RVAL_GETTER, CLASS, __VA_ARGS__)                      \
  template<size_t I> constexpr auto & get(CLASS &obj);              \
  INSTANTIATE(NON_GETTER, CLASS, __VA_ARGS__)                       \
  template<size_t I> constexpr const auto & get(const CLASS &obj);  \
  INSTANTIATE(CONST_GETTER, CLASS, __VA_ARGS__)                     \
}

#ifndef TERAKI_DEV_UTILS_DATA_TYPE_HPP
#define TERAKI_DEV_UTILS_DATA_TYPE_HPP

#include <string>
#include <tuple>
#include <type_traits>

#include "teraki/dev/utils/for_each.hpp"

namespace teraki {
namespace dev {

template<typename T>
constexpr uint8_t set_floating_bit(const uint8_t val) {
  return val | static_cast<uint8_t>(std::is_floating_point<T>::value << 6u);
}

template<typename T>
constexpr uint8_t set_signed_bit(const uint8_t val) {
  return val | static_cast<uint8_t>(std::is_signed<T>::value << 7u);
}

template<typename T>
constexpr uint8_t data_type() {
  return set_signed_bit<T>(set_floating_bit<T>(sizeof(T)));
}

template<uint8_t val>
struct DataType {
  static_assert(val >= 0, "We have not defined a data type code for this value");
};

template<>
struct DataType<data_type<uint8_t>()> {
  using type = uint8_t;
  static constexpr const char *name() { return "uint8"; }
};

template<>
struct DataType<data_type<uint16_t>()> {
  using type = uint16_t;
  static constexpr const char *name() { return "uint16"; }
};

template<>
struct DataType<data_type<uint32_t>()> {
  using type = uint32_t;
  static constexpr const char *name() { return "uint32"; }
};

template<>
struct DataType<data_type<uint64_t>()> {
  using type = uint64_t;
  static constexpr const char *name() { return "uint64"; }
};

template<>
struct DataType<data_type<int8_t>()> {
  using type = int8_t;
  static constexpr const char *name() { return "int8"; }
};

template<>
struct DataType<data_type<int16_t>()> {
  using type = int16_t;
  static constexpr const char *name() { return "int16"; }
};

template<>
struct DataType<data_type<int32_t>()> {
  using type = int32_t;
  static constexpr const char *name() { return "int32"; }
};

template<>
struct DataType<data_type<int64_t>()> {
  using type = int64_t;
  static constexpr const char *name() { return "int64"; }
};

template<>
struct DataType<set_signed_bit<float>(set_floating_bit<float>(4))> {
  using type = float;
  static_assert(sizeof(float) == 4, "Oops. A float is not 32 bits on your system");
  static constexpr const char *name() { return "float"; }
};

template<>
struct DataType<set_signed_bit<double>(set_floating_bit<double>(8))> {
  using type = double;
  static_assert(sizeof(double) == 8, "Oops. A double is not 64 bits on your system");
  static constexpr const char *name() { return "double"; }
};

template<typename T>
inline constexpr const char *data_type_name() {
  return DataType<data_type<T>()>::name();
}

inline uint8_t data_type(const std::string &type) {

  // TODO(matt): The integer types should be replaced by a for_each on the tuple.
  //
  // using types = std::tuple<uint8_t, int8_t, uint16_t, int16_t, uint32_t, int32_t, uint64_t, int64_t>;

  if (type == "float" || type == "float32") {
    return teraki::dev::data_type<float>();
  }
  if (type == "double" || type == "float64") {
    return teraki::dev::data_type<double>();
  }
  if (type == "char") {
    return teraki::dev::data_type<int8_t>();
  }
  if (type == "uchar") {
    return teraki::dev::data_type<uint8_t>();
  }
  if (type == data_type_name<uint8_t>()) {
    return teraki::dev::data_type<uint8_t>();
  }
  if (type == data_type_name<uint16_t>()) {
    return teraki::dev::data_type<uint16_t>();
  }
  if (type == data_type_name<uint32_t>()) {
    return teraki::dev::data_type<uint32_t>();
  }
  if (type == data_type_name<uint64_t>()) {
    return teraki::dev::data_type<uint64_t>();
  }
  if (type == data_type_name<int8_t>()) {
    return teraki::dev::data_type<int8_t>();
  }
  if (type == data_type_name<int16_t>()) {
    return teraki::dev::data_type<int16_t>();
  }
  if (type == data_type_name<int32_t>()) {
    return teraki::dev::data_type<int32_t>();
  }
  if (type == data_type_name<int64_t>()) {
    return teraki::dev::data_type<int64_t>();
  }
  return 0;
}

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_DATA_TYPE_HPP */

#ifndef TERAKI_DEV_UTILS_CLUSTER_HPP
#define TERAKI_DEV_UTILS_CLUSTER_HPP

#include <memory>
#include <tuple>
#include <vector>

#include "teraki/dev/utils/tuple_logical_expressions.hpp"
#include "teraki/dev/utils/endl.hpp"
#include "teraki/dev/utils/is_valid.hpp"

namespace teraki {
namespace dev {

template<typename PC>
struct ClusterTree {

 public:

  using ClusterContainer = std::vector<PC>;
  using Point = std::decay_t<typename PC::value_type>;

  ClusterContainer clusters;
  Point tolerances;

 private:

  struct ClusterNode {

    Point pivot;

    // The index of the cluster (in the ClusterTree's ClusterContainer)
    // containing all of the points within the
    // specified tolerance of the pivot (including the pivot).
    // This should not be a pointer or reference because the underlying
    // container may resize during clustering, invalidating such a choice
    size_t cluster_index;

    std::vector<ClusterNode> left;
    std::vector<ClusterNode> right;

    ClusterNode(const Point &pivot, ClusterContainer &clusters) : pivot(pivot), cluster_index(clusters.size()) {
      clusters.resize(clusters.size() + 1);
      clusters.back().push_back(pivot);
      left.reserve(1);
      right.reserve(1);
    }
  };

 public:

  ClusterNode root;

 private:

  template<size_t I, typename>
  struct Distance {
    static constexpr auto run(const Point &p1, const Point &p2) {
      return std::get<I>(p1) > std::get<I>(p2)
             ? std::get<I>(p1) - std::get<I>(p2)
             : std::get<I>(p2) - std::get<I>(p1);
    }
  };

  template<size_t I, typename>
  struct WithinTolerances {
    static constexpr bool run(const Point &p1, const Point &p2, const Point &tol) {
      return Distance<I, Point>::run(p1, p2) <= std::get<I>(tol);
    }
  };

  struct Matcher {

    std::vector<ClusterNode> *best_insertion_point = nullptr;
    double best_distance;

    void update(double distance, std::vector<ClusterNode> &insertion_point) {
      if (best_insertion_point == nullptr || distance < best_distance) {
        best_insertion_point = &insertion_point;
        best_distance = distance;
      }
    }
  };

 public:

  void insert(const Point &point) {
    Matcher matcher;
    if (!insert(point, root, matcher)) {
      // The point was not inserted into one of the preallocated clusters.
      // Create a new ClusterNode at the best_insertion_point
      (*matcher.best_insertion_point).push_back(ClusterNode(point, clusters));
    }
  }

 private:

  // Try to insert the specified point at the specified node.
  // Return true if it was inserted.
  // Otherwise update the matcher with the best place to insert the point
  template<size_t axis = 0>
  bool insert(const Point &point, ClusterNode &node, Matcher &matcher) {

    // If the point is within the specified tolerance of the node's pivot,
    // then add it to its cluster of points.
    if (teraki::dev::all<Point, WithinTolerances>(point, node.pivot, tolerances)) {
      clusters[node.cluster_index].push_back(point);
      return true;
    }

    // To determine whether to examine the left or right branch next, we look whether
    // the point is to the left or right of the node's pivot point on the current axis
    const bool left_of_pivot = std::get<axis>(point) < std::get<axis>(node.pivot);
    auto &closer_branch = left_of_pivot ? node.left : node.right;

    // Try to insert on the closer branch
    constexpr size_t next_axis = axis == std::tuple_size<Point>::value - 1 ? 0 : axis + 1;
    if (!closer_branch.empty() && insert<next_axis>(point, closer_branch.front(), matcher)) {
      // Inserted somewhere in the closer branch. Done.
      return true;
    }

    // Try to insert on the farther branch.
    auto &farther_branch = left_of_pivot ? node.right : node.left;
    const auto dist = Distance<axis, Point>::run(point, node.pivot);
    if (!farther_branch.empty()) {

      // If the distance form the point to the pivot is larger than the tolerance
      // along this axis then we don't have to search the other branch.
      // All of the other points are invalid
      if (dist <= std::get<axis>(tolerances) && insert<next_axis>(point, farther_branch.front(), matcher)) {
        // Inserted somewhere in the farther branch. Done.
        return true;
      }
    }

    // The point was still not inserted. If the closer branch is empty,
    // then we could potentially insert this point there
    matcher.update(dist, closer_branch);
    return false;
  }

 public:

  // You need to initialize ClusterTree with a point. That point will be the pivot of the root
  ClusterTree(const Point &point,
              const Point &tolerances)
      : tolerances(tolerances),
        root(point, clusters) {
  }
};

template<typename PC>
inline auto cluster(const PC &pc,
                    const std::decay_t<typename PC::value_type> &tolerances) {
  if (pc.empty()) {
    return typename ClusterTree<PC>::ClusterContainer();
  }
  auto point = std::begin(pc);
  auto end = std::end(pc);
  ClusterTree<PC> tree(*point, tolerances);
  for (std::advance(point, 1); point != end; std::advance(point, 1)) {
    tree.insert(*point);
  }
  return tree.clusters;
}

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_CLUSTER_HPP */

#ifndef TERAKI_DEV_UTILS_TIMER_HPP
#define TERAKI_DEV_UTILS_TIMER_HPP

#include <chrono>
#include <iostream>

#include "teraki/dev/utils/endl.hpp"

namespace teraki {
namespace dev {

template<typename Units>
struct Timer {

  std::chrono::high_resolution_clock::time_point start_time;
  std::chrono::high_resolution_clock::time_point stop_time;
  decltype(std::chrono::duration_cast<Units>(stop_time - start_time)) duration;
  std::string pretime_message;
  std::string posttime_message;

  Timer() : pretime_message(""), posttime_message("") {
    // Reset and start the timer on construction
    reset();
    resume();
    stop_time = start_time;
  }

  void resume() {
    start_time = std::chrono::high_resolution_clock::now();
  }

  void restart() {
    reset();
    resume();
  }

  void start() {
    resume();
  }

  void pause() {
    stop_time = std::chrono::high_resolution_clock::now();
    duration += std::chrono::duration_cast<Units>(stop_time - start_time);
  }

  void stop() {
    pause();
  }

  void reset() {
    duration = decltype(duration)::zero();
  }

  void print() {
    std::cout << pretime_message << duration.count() << posttime_message << endl;
  }

  void print(const std::string &pretime_message,
             const std::string &posttime_message) {
    std::cout << pretime_message << duration.count() << posttime_message << endl;
  }
};

using MilliTimer = Timer<std::chrono::milliseconds>;
using MicroTimer = Timer<std::chrono::microseconds>;
using NanoTimer = Timer<std::chrono::nanoseconds>;

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_TIMER_HPP */

#ifndef TERAKI_DEV_UTILS_NEAREST_NEIGHBOR_HPP
#define TERAKI_DEV_UTILS_NEAREST_NEIGHBOR_HPP

#include <queue>
#include <tuple>
#include <type_traits>

#include "teraki/dev/goods/node.hpp"
#include "teraki/dev/utils/for_each.hpp"
#include "teraki/dev/utils/kdtree.hpp"
#include "teraki/dev/utils/tuple_logical_expressions.hpp"
#include "teraki/dev/utils/unused.hpp"

namespace teraki {
namespace dev {

template<typename PC>
struct NearestNeighbor {

 public:

  static_assert(teraki::dev::is_valid_point_cloud<PC>(), "PointCloud is not valid.");
  using PointType = typename PC::value_type;
  static constexpr size_t dimensions = std::tuple_size<PointType>::value;
  const Node<PointType> main_tree;

 private:

  static constexpr bool default_constraints(const PointType &point) {
    unused::sink(point);
    return true;
  }

  template<size_t I, typename>
  struct DistanceSquared {

    template<typename Point,
        std::enable_if_t<std::is_unsigned<std::tuple_element_t<I, Point>>::value> *unused = nullptr>
    static constexpr auto run(const Point &p1, const Point &p2) {
      if (std::get<I>(p1) > std::get<I>(p2)) {
        const auto diff = std::get<I>(p1) - std::get<I>(p2);
        return diff * diff;
      }
      const auto diff = std::get<I>(p2) - std::get<I>(p1);
      return diff * diff;
    }

    template<typename Point,
        std::enable_if_t<!std::is_unsigned<std::tuple_element_t<I, Point>>::value> *unused = nullptr>
    static constexpr auto run(const Point &p1, const Point &p2) {
      return std::pow(std::get<I>(p1) - std::get<I>(p2), 2);
    }

    template<typename Axis>
    static constexpr auto run(const Axis &axis, const PointType &p1, const PointType &p2) {
      unused::sink(axis);
      return run(p1, p2);
    }
  };

  template<typename Distance>
  struct Match {

   private:
    PointType point;
    Distance distance;

   public:
    Match(PointType point,
          Distance distance) :
        point(std::move(point)), distance(std::move(distance)) {}

    const PointType &get_point() const {
      return point;
    }

    const Distance &get_distance() const {
      return distance;
    }

  };

  template<typename Constraints, typename Distance = double>
  struct Matcher {

   private:

    struct MatchComparator {

      bool operator()(const Match<Distance> &lhs, const Match<Distance> &rhs) {
        return lhs.get_distance() < rhs.get_distance();
      }
    };
    std::priority_queue<Match<Distance>, std::vector<Match<Distance>>, MatchComparator> best_matches;

   public:

    const Constraints &constraints;
    const PointType &reference_point;
    const size_t num_matches;

    Matcher(const Constraints &constraints, const PointType &reference_point, size_t num_matches)
        : constraints(constraints), reference_point(reference_point), num_matches(num_matches) {
    }

    // Update the list of best matches
    void update(const PointType &point) {

      // If the point satisfies the constraints.
      if (constraints(point)) {

        // Compute the distance to the reference point
        Distance distance = teraki::dev::for_each<PointType, DistanceSquared, for_each_operations::Sum>(
            reference_point, point);

        // If we have enough matches, then we need to check if this is better than the best so far.
        if (has_enough_matches()) {

          if (distance < distance_to_beat()) {
            // Remove the worst so far
            best_matches.pop();
            // and add the new point
            best_matches.emplace(point, distance);
          }
        } else {

          // Otherwise, just add the point to the best matches
          best_matches.emplace(point, distance);

        }
      }
    }

    // Create a vector of matches with just the points (not distances)
    auto matches() {
      auto best_matches_copy = best_matches;
      std::vector<PointType> out;
      out.reserve(best_matches.size());
      while (!best_matches_copy.empty()) {
        out.push_back(best_matches_copy.top().get_point());
        best_matches_copy.pop();
      }
      return out;
    }

    Distance distance_to_beat() {
      return best_matches.top().get_distance();
    }

    bool has_enough_matches() {
      return best_matches.size() == num_matches;
    }
  };

// Constraints which return true if the point is within the specified per-axis distance to the reference point
  struct ToleranceConstraints {

   public:

    const PointType reference_point;
    const PointType tolerances;

    ToleranceConstraints(PointType reference_point,
                         PointType tolerances)
        : reference_point(std::move(reference_point)),
          tolerances(std::move(tolerances)) {
    }

   private:

    template<size_t I, typename>
    struct Comparison {

      static constexpr bool
      run(const PointType &reference_point, const PointType &point, const PointType &tolerances) {
        if (std::get<I>(reference_point) > std::get<I>(point)) {
          return (std::get<I>(reference_point) - std::get<I>(point)) <= std::get<I>(tolerances);
        }
        return (std::get<I>(point) - std::get<I>(reference_point)) <= std::get<I>(tolerances);
      }
    };

   public:

    bool operator()(const PointType &point) const {
      return teraki::dev::all<PointType, Comparison>(reference_point, point, tolerances);
    }
  };

 public:

  explicit NearestNeighbor(const PC &pc) : main_tree(kdtree(pc)) {}

  template<typename Constraints>
  std::vector<PointType> search(const PointType &reference_point,
                                size_t num_matches,
                                Constraints constraints) {

    Matcher<Constraints> matcher(constraints, reference_point, num_matches);
    search_impl(main_tree, matcher);
    return matcher.matches();
  }

  std::vector<PointType> search(const PointType &reference_point,
                                size_t num_matches) {
    return search(reference_point, num_matches, default_constraints);
  }

  std::vector<PointType> search_within_tolerances(const PointType &reference_point,
                                                  size_t num_matches,
                                                  const PointType &tolerances) {

    ToleranceConstraints tolerance_constraints(reference_point, tolerances);
    return search(reference_point, num_matches, tolerance_constraints);
  }

 private:

  /**
   * Find the point in the tree which is closest to the matcher's reference point
   */
  template<typename Matcher, size_t axis = 0>
  void search_impl(const Node<PointType> &tree,
                   Matcher &matcher) {

    if (tree.has_children()) {

      // Check whether the reference point is on the left or right of the pivot
      const bool left_of_pivot = std::get<axis>(matcher.reference_point) < std::get<axis>(tree.pivot());
      if (left_of_pivot) {

        if (tree.valid_left()) {
          search_impl<Matcher, axis == dimensions - 1 ? 0 : axis + 1>(tree.left(), matcher);
        }

        if (tree.valid_right()) {

          // If we don't have enough matches yet, or if the distance between the
          // reference point and the pivot is less than or equal to the distance to beat
          // then we need to check the right branch too
          // TODO(matt) This should use the generic distance function
          const auto abs_distance_to_pivot = std::get<axis>(tree.pivot()) - std::get<axis>(matcher.reference_point);
          const auto distance_to_pivot = abs_distance_to_pivot * abs_distance_to_pivot;
          if (!matcher.has_enough_matches() || distance_to_pivot < matcher.distance_to_beat()) {
            search_impl<Matcher, axis == dimensions - 1 ? 0 : axis + 1>(tree.right(), matcher);
          }
        }

      } else {

        if (tree.valid_right()) {
          search_impl<Matcher, axis == dimensions - 1 ? 0 : axis + 1>(tree.right(), matcher);
        }

        if (tree.valid_left()) {

          // If we don't have enough matches yet, or if the distance between the
          // reference point and the pivot is less than or equal to the distance to beat
          // then we need to check the right branch too
          // TODO(matt) This should use the generic distance function
          const auto abs_distance_to_pivot = std::get<axis>(matcher.reference_point) - std::get<axis>(tree.pivot());
          const auto distance_to_pivot = abs_distance_to_pivot * abs_distance_to_pivot;
          if (!matcher.has_enough_matches() || distance_to_pivot < matcher.distance_to_beat()) {
            search_impl<Matcher, axis == dimensions - 1 ? 0 : axis + 1>(tree.left(), matcher);
          }
        }

      }
    }

    // We also need to check whether the pivot is the best point.
    matcher.update(tree.pivot());
  }
};

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_NEAREST_NEIGHBOR_HPP */

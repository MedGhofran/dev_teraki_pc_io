#ifndef TERAKI_DEV_UTILS_KDTREE_HPP
#define TERAKI_DEV_UTILS_KDTREE_HPP

#include <algorithm>
#include <tuple>

#include "teraki/dev/goods/node.hpp"
#include "teraki/dev/utils/is_valid.hpp"

namespace teraki {
namespace dev {

template<typename Iterator, size_t axis = 0>
inline auto kdtree(Iterator begin, Iterator end) {

  using Point = typename Iterator::value_type;

  const auto size = std::distance(begin, end);
  if (size == 0) {
    return Node<Point>();
  }
  if (size == 1) {
    return Node<Point>(*begin);
  }

  // Sort the point cloud along the specified axis
  std::sort(begin, end, [](const auto &p1, const auto &p2) { return std::get<axis>(p1) < std::get<axis>(p2); });

  // Get the left point cloud, pivot point (median), and right point clouds
  const auto left_size = size / 2;
  const auto right_size = size - left_size - 1;
  auto pivot = std::next(begin, left_size);
  constexpr auto dimensions = std::tuple_size<Point>::value;
  if (right_size == 0) {
    return Node<Point>(*pivot, kdtree<Iterator, axis == dimensions - 1 ? 0 : axis + 1>(begin, pivot));
  }
  auto middle = std::next(pivot);
  return Node<Point>(*pivot,
                     kdtree<Iterator, axis == dimensions - 1 ? 0 : axis + 1>(begin, pivot),
                     kdtree<Iterator, axis == dimensions - 1 ? 0 : axis + 1>(middle, end));
}

template<typename PC, size_t axis = 0>
inline auto kdtree(PC &pc) {
  return kdtree(begin(pc), end(pc));
}

template<typename PC, size_t axis = 0>
inline auto kdtree(const PC &point_cloud) {
  PC pc = point_cloud;
  return kdtree(pc);
}

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_KDTREE_HPP */

#ifndef TERAKI_DEV_UTILS_NUMERICAL_CHARS_HPP
#define TERAKI_DEV_UTILS_NUMERICAL_CHARS_HPP

#include <istream>
#include <ostream>

namespace teraki {
namespace dev {

namespace numerical_chars {

inline std::ostream &operator<<(std::ostream &ostream, int8_t c) {
  return ostream << static_cast<int16_t>(c);
}

inline std::ostream &operator<<(std::ostream &ostream, uint8_t c) {
  return ostream << static_cast<uint16_t>(c);
}

inline std::istream &operator>>(std::istream &istream, int8_t &num) {
  // We make no effort to make sure that this is indeed a uint8_t!!!
  int16_t temp;
  istream >> temp;
  num = static_cast<int8_t>(temp);
  return istream;
}

inline std::istream &operator>>(std::istream &istream, uint8_t &num) {
  // We make no effort to make sure that this is indeed a uint8_t!!!
  uint16_t temp;
  istream >> temp;
  num = static_cast<uint8_t>(temp);
  return istream;
}

} // namespace numerical_chars
} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_NUMERICAL_CHARS_HPP */
#ifndef TERAKI_DEV_UTILS_PRINT_HPP
#define TERAKI_DEV_UTILS_PRINT_HPP

#include <iomanip>
#include <iostream>
#include <limits>
#include <ostream>
#include <sstream>
#include <string>

#include "teraki/dev/utils/endl.hpp"
#include "teraki/dev/utils/is_valid.hpp"
#include "teraki/dev/utils/numerical_chars.hpp"

namespace teraki {
namespace dev {

// TODO(matt): These methods should use validity checks in the functions. Probably static asserts

template<typename Point>
inline void print_point(const Point &point,
                        std::ostream &ostream,
                        const std::string &separator,
                        const std::string &endl_string) {
  bool initialized = false;
  teraki::dev::for_each_lambda(point, [&ostream, &initialized, &separator](const auto &val) {
    if (initialized) {
      ostream << separator;
    } else {
      initialized = true;
    }
    {
      using numerical_chars::operator<<;
      ostream << std::setprecision(std::numeric_limits<std::decay_t<decltype(val)>>::digits10 + 2) << val;
    }
  });
  ostream << endl_string;
}

template<typename Point>
inline void print_point(const Point &point,
                        std::ostream &ostream,
                        const std::string &separator) {
  print_point(point, ostream, separator, endl);
}

template<typename Point>
inline void print_point(const Point &point,
                        std::ostream &ostream) {
  static const std::string separator = ", ";
  print_point(point, ostream, separator);
}

template<typename Point>
inline void print_point(const Point &point) {
  print_point(point, std::cout);
}

template<typename PointCloud>
void print(const PointCloud &pc) {
  for (const auto &point : pc) {
    print_point(point);
  }
}

template<typename PointCloud>
void print(const PointCloud &pc, std::ostream &ostream) {
  for (const auto &point : pc) {
    print_point(point, ostream);
  }
}

template<typename PointCloud>
void print(const PointCloud &pc, std::ostream &ostream, const std::string &separator) {
  for (const auto &point : pc) {
    print_point(point, ostream, separator);
  }
}

template<typename PointCloud>
void print(const PointCloud &pc,
           std::ostream &ostream,
           const std::string &separator,
           const std::string &endl_string) {
  for (const auto &point : pc) {
    print_point(point, ostream, separator, endl_string);
  }
}

template<typename PointCloud>
std::string to_string(const PointCloud &pc) {
  std::stringstream ss;
  print(pc, ss);
  return ss.str();
}

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_PRINT_HPP */

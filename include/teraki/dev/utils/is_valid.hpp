#ifndef TERAKI_DEV_UTILS_IS_VALID_HPP
#define TERAKI_DEV_UTILS_IS_VALID_HPP

#include <tuple>
#include <type_traits>

#include "teraki/dev/utils/for_each.hpp"

namespace teraki {
namespace dev {

template<size_t I, typename Point>
struct PointValidityChecker {
  constexpr static void run() {
    static_assert(std::is_same<std::tuple_element_t<I, Point>,
                               std::remove_reference_t<decltype(std::get<I>(std::declval<Point>()))>>::value,
                  "std::get<i>(PointType) should get the ith element of a point for all i = 0,..,std::tuple_size<PointType>::value - 1"
                  "and it should have the same type as std::tuple_element<i,PointType>::type"
    );
  }
};

template<typename T>
struct IsValid {

  constexpr bool point() { return true; }
  constexpr bool point_cloud() { return true; }
};

template<typename PointType>
constexpr bool is_valid_point() {

  // Check whether std::tuple_size can be called on the type.
  // TODO(matt): This is not actually going to raise the assert, but the user WILL see the error message
  static_assert(std::tuple_size<PointType>::value >= 0,
                "You have not specified a valid point type. Typically, Points are either std::arrays or std::tuples."
                "To be valid, std::tuple_size<PointType>::value must define the compile-time size of the point type.");

  // Check whether the std::tuple_size > 0
  static_assert(std::tuple_size<PointType>::value > 0,
                "You have not specified a valid point type. "
                "The PointCloud point type must have a size greater than 0");

  // Finally check the individual entries
  teraki::dev::for_each<PointType, PointValidityChecker>();
  return true;
}

template<typename PointCloud>
constexpr bool is_valid_point_cloud() {

  using PC = std::decay_t<PointCloud>;
  using Point = typename PC::value_type;

  // First, check whether the PointType is valid
  static_assert(teraki::dev::is_valid_point<Point>(),
                "The PointCloud must have a value_type alias for the PointType. Either you haven't specified that, or you have not specified a valid point type.");

  // Furthermore, the cloud must provide a few operators that, roughly speaking look like this:
  //  value_type &operator[](std::size_t idx) { return data[idx]; }
  static_assert(std::is_same<Point &, decltype(std::declval<PC>()[0])>::value,
                "The PointCloud does not seem to have a point index operation like pc[i] to get a non-const reference to the i^th point");

  //  const value_type &operator[](std::size_t idx) const { return data[idx]; }
  static_assert(std::is_same<const Point &, decltype(std::declval<const PC>()[0])>::value,
                "The PointCloud does not seem to have a point index operation like pc[i] to get a const reference to the i^th point");

  //  size_type size() const { return data.size(); }
  static_assert(std::is_same<decltype(std::declval<const PC>().size()),
                             decltype(std::declval<const PC>().size())>::value,
                "The Point cloud does not seem to have a 'size' operation that works on const PointClouds");

  //  auto begin() -> decltype(data.begin()) { return data.begin(); }
  static_assert(std::is_same<decltype(std::declval<PC>().begin()),
                             decltype(std::declval<PC>().begin())>::value,
                "The Point cloud does not seem to have a 'begin' operation that works on non-const PointClouds");

  //  auto end() -> decltype(data.end()) { return data.end(); }
  static_assert(std::is_same<decltype(std::declval<PC>().end()),
                             decltype(std::declval<PC>().end())>::value,
                "The Point cloud does not seem to have a 'end' operation that works on non-const PointClouds");

  //  void clear() { data.clear();  }
  static_assert(std::is_same<decltype(std::declval<PC>().clear()),
                             decltype(std::declval<PC>().clear())>::value,
                "The Point cloud does not seem to have a 'clear' operation that works on non-const PointClouds");

  //  void resize(size_t n) { data.resize(n); }
  static_assert(std::is_same<decltype(std::declval<PC>().resize(0)),
                             decltype(std::declval<PC>().resize(0))>::value,
                "The Point cloud does not seem to have a 'resize' operation that works on non-const PointClouds");

  //  void push_back(const value_type &val) { data.push_back(val); }
  static_assert(std::is_same<decltype(std::declval<PC>().push_back(std::declval<PC>()[0])),
                             decltype(std::declval<PC>().push_back(std::declval<PC>()[0]))>::value,
                "The Point cloud does not seem to have a 'push_back' operation that works on non-const PointClouds");

  return true;
}

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_IS_VALID_HPP */

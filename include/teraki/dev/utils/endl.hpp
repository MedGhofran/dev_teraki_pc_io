#ifndef TERAKI_DEV_UTILS_ENDL_HPP
#define TERAKI_DEV_UTILS_ENDL_HPP

namespace teraki {
namespace dev {
constexpr char const *endl = "\n";
} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_ENDL_HPP */

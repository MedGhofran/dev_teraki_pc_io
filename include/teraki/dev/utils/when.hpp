#ifndef TERAKI_DEV_UTILS_WHEN_HPP
#define TERAKI_DEV_UTILS_WHEN_HPP

#include <tuple>

#include "teraki/dev/utils/unused.hpp"

namespace teraki {
namespace dev {

template<size_t I, typename Tuple>
struct AtIndex {

  template<typename ...Args>
  static constexpr bool run(const size_t axis, Args &&...args) {
    unused::sink(std::forward<Args>(args)...);
    return I == axis;
  }
};

template<size_t I, typename Tuple>
struct LessThan {
  template<typename Other>
  static constexpr bool run(Other &&, const Tuple &p1, const Tuple &p2) {
    return std::get<I>(p1) < std::get<I>(p2);
  }
};

// Iterate over the tuple until the condition is met.
// When the condition is met, return the result of the functor

template<
    typename Tuple,
    template<size_t, typename> class Condition,
    template<size_t, typename> class Functor,
    size_t I = 0,
    typename ReturnType,
    std::enable_if_t<I >= std::tuple_size<Tuple>::value> *unused = nullptr,
    typename ...Args>
constexpr ReturnType when(const ReturnType &default_return_value, Args &&...args) {
  unused::sink(std::forward<Args>(args)...);
  return default_return_value;
}

template<
    typename Tuple,
    template<size_t, typename> class Condition,
    template<size_t, typename> class Functor,
    size_t I = 0,
    typename ReturnType,
    std::enable_if_t<I < std::tuple_size<Tuple>::value> *unused = nullptr,
    typename ...Args>
constexpr ReturnType when(const ReturnType &default_return_value, Args &&...args) {
  if (Condition<I, Tuple>::run(std::forward<Args>(args)...)) {
    return Functor<I, Tuple>::run(std::forward<Args>(args)...);
  }
  return when<Tuple, Condition, Functor, I + 1>(default_return_value, std::forward<Args>(args)...);
}

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_WHEN_HPP */
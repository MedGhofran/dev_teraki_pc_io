#ifndef TERAKI_DEV_UTILS_FILLERS_HPP
#define TERAKI_DEV_UTILS_FILLERS_HPP

#include <random>
#include <array>
#include <tuple>

#include "teraki/dev/utils/for_each.hpp"
#include "teraki/dev/utils/is_valid.hpp"

namespace teraki {
namespace dev {

struct PointFiller {

 private:

  static size_t count(bool reset) {
    static size_t c = 0;
    if (reset) {
      c = 0;
    }
    return c++;
  }

  static std::mt19937 &engine(bool reset) {
    static std::mt19937 engine;
    if (reset) {
      engine = std::mt19937{};
    }
    return engine;
  }

  static double uniform_generator() {
    static std::uniform_real_distribution<double> dist;
    return dist(engine(false));
  }

  static double normal_generator() {
    static std::normal_distribution<double> dist;
    return dist(engine(false));
  }

 public:

  template<typename Point>
  static Point &uniform(Point &point) {
    static_assert(teraki::dev::is_valid_point<Point>(), "PointType is not valid.");
    teraki::dev::for_each_lambda(point, [](auto &val) { val = uniform_generator(); });
    return point;
  }

  template<typename Point>
  static Point uniform() {
    Point point;
    uniform(point);
    return point;
  }

  template<typename Point>
  static Point &normal(Point &point) {
    static_assert(teraki::dev::is_valid_point<Point>(), "PointType is not valid.");
    teraki::dev::for_each_lambda(point, [](auto &val) { val = normal_generator(); });
    return point;
  }

  template<typename Point>
  static Point normal() {
    Point point;
    normal(point);
    return point;
  }

  template<typename Point>
  static Point &counting(Point &point) {
    static_assert(teraki::dev::is_valid_point<Point>(), "PointType is not valid.");
    teraki::dev::for_each_lambda(point, [](auto &val) { val = count(false); });
    return point;
  }

  template<typename Point, typename Scale>
  static Point &counting(Point &point, Scale scale) {
    static_assert(teraki::dev::is_valid_point<Point>(), "PointType is not valid.");
    teraki::dev::for_each_lambda(point, [scale](auto &val) { val = scale * count(false); });
    return point;
  }

  template<typename Point>
  static Point counting() {
    Point point;
    counting(point);
    return point;
  }

  static void reset() {
    engine(true);
    count(true);
  }
};

struct PointCloudFiller {

 public:

  template<typename PC>
  static PC &uniform(PC &point_cloud) {
    static_assert(teraki::dev::is_valid_point_cloud<PC>(), "PointCloud is not valid.");
    for (auto &point : point_cloud) {
      PointFiller::uniform(point);
    }
    return point_cloud;
  }

  template<typename PC>
  static PC &normal(PC &point_cloud) {
    static_assert(teraki::dev::is_valid_point_cloud<PC>(), "PointCloud is not valid.");
    for (auto &point : point_cloud) {
      PointFiller::normal(point);
    }
    return point_cloud;
  }

  template<typename PC, typename Scale>
  static PC &counting(PC &point_cloud, Scale scale) {
    static_assert(teraki::dev::is_valid_point_cloud<PC>(), "PointCloud is not valid.");
    for (auto &point : point_cloud) {
      PointFiller::counting(point, scale);
    }
    return point_cloud;
  }

  template<typename PC>
  static PC &counting(PC &point_cloud) {
    static_assert(teraki::dev::is_valid_point_cloud<PC>(), "PointCloud is not valid.");
    constexpr auto scale = 1.f / std::tuple_size<typename PC::value_type>::value;
    for (auto &point : point_cloud) {
      PointFiller::counting(point, scale);
    }
    return point_cloud;
  }

  template<typename PC>
  static PC &counting_bounded(PC &point_cloud) {
    static_assert(teraki::dev::is_valid_point_cloud<PC>(), "PointCloud is not valid.");
    const auto scale = 1.f / point_cloud.size();
    for (auto &point : point_cloud) {
      PointFiller::counting(point, scale);
    }
    return point_cloud;
  }

  static void reset() {
    PointFiller::reset();
  }
};

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_FILLERS_HPP */

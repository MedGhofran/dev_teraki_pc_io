#ifndef TERAKI_DEV_UTILS_UNUSED_HPP
#define TERAKI_DEV_UTILS_UNUSED_HPP

namespace teraki {
namespace dev {
namespace unused {

constexpr void sink() {
}

template<typename T>
constexpr void sink(const T &t) {
  (void) t;
}

template<typename T, typename ...Args>
constexpr void sink(const T &t, Args &&...args) {
  sink(t);
  sink(std::forward<Args>(args)...);
}
} // namespace unused
} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_UNUSED_HPP */
#pragma once

template<typename precise_float = double, typename PointType>
inline auto range(const PointType &min, const PointType &max) {
  return static_cast<precise_float>(max) - static_cast<precise_float>(min);
}
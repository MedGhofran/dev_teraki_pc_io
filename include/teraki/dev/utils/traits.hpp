#ifndef TERAKI_DEV_UTILS_TRAITS_HPP
#define TERAKI_DEV_UTILS_TRAITS_HPP

namespace teraki {
namespace dev {

// The type alias in this class is equal to T::value_type if T has such an alias.
// Otherwise, the type alias is equal to the Alternative
template<typename T, typename Alternative>
struct ValueType {
 private:

  template<typename U>
  constexpr static auto test(int) -> typename U::value_type;

  template<typename>
  constexpr static Alternative test(...);

 public:

  using type = decltype(test<T>(0));
};

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_TRAITS_HPP */
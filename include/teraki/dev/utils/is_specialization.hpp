#ifndef TERAKI_DEV_UTILS_IS_SPECIALIZATION_HPP
#define TERAKI_DEV_UTILS_IS_SPECIALIZATION_HPP

namespace teraki {
namespace dev {

template<typename Test, template<typename...> class Ref>
struct is_specialization : std::false_type {};

template<template<typename...> class Ref, typename... Args>
struct is_specialization<Ref<Args...>, Ref> : std::true_type {};

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_IS_SPECIALIZATION_HPP */

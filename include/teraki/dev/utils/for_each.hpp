#ifndef TERAKI_DEV_UTILS_FOR_EACH_HPP
#define TERAKI_DEV_UTILS_FOR_EACH_HPP

#include <tuple>

#include "teraki/dev/utils/unused.hpp"

namespace teraki {
namespace dev {

template<typename T>
constexpr bool is_void() {
  return std::is_same<void, T>::value;
}

namespace for_each_operations {

struct Nothing {
  template<typename ...Args>
  static constexpr void run(Args &&... args) {
    unused::sink(std::forward<Args>(args)...);
  }
};

struct Sum {

  static constexpr void run() {
  }

  template<typename T>
  static constexpr auto run(T &&t) {
    return t;
  }

  template<typename T, typename U>
  static constexpr auto run(T &&t, U &&u) {
    return t + u;
  }
};

} // namespace for_each_operations

// This class of methods evaluates the provided function for each value in the tuple
template<typename Tuple, typename Function, size_t I = 0>
constexpr std::enable_if_t<I == std::tuple_size<Tuple>::value>
for_each_lambda(const Tuple &tuple, const Function &function) {
  unused::sink(tuple);
  unused::sink(function);
}

template<typename Tuple, typename Function, size_t I = 0>
constexpr std::enable_if_t<I < std::tuple_size<Tuple>::value>
for_each_lambda(Tuple &t, Function f) {
  f(std::get<I>(t));
  for_each_lambda<Tuple, Function, I + 1>(t, f);
}

// This class of methods evaluates the provided function for each value in the tuple
template<typename Tuple, typename Function, size_t I = 0>
constexpr std::enable_if_t<I == std::tuple_size<Tuple>::value>
for_each_lambda_enumerate(const Tuple &tuple, const Function &function) {
  unused::sink(tuple);
  unused::sink(function);
}

template<typename Tuple, typename Function, size_t I = 0>
constexpr std::enable_if_t<I < std::tuple_size<Tuple>::value>
for_each_lambda_enumerate(Tuple &t, Function f) {
  f(std::get<I>(t), std::integral_constant<size_t, I>());
  for_each_lambda_enumerate<Tuple, Function, I + 1>(t, f);
}

// This never gets called unless you try to do something on a tuple of size 0
template<
    typename Tuple,
    template<size_t, typename> class Functor,
    typename Operation = for_each_operations::Nothing,
    size_t I = 0,
    std::enable_if_t<std::tuple_size<Tuple>::value == 0> *unused = nullptr,
    typename ...Args>
constexpr void for_each(Args &&...args) {
  unused::sink(std::forward<Args>(args)...);
  return Operation::run();
}

// On the last iteration, we just return the operation as a function of the last argument
// We have to handle two cases, depending on whether the functor returns a void.
// If the Functor returns a void, then we run the functor and return the result of the
// Operation with no arguments
template<
    typename Tuple,
    template<size_t, typename> class Functor,
    typename Operation = for_each_operations::Nothing,
    size_t I = 0,
    std::enable_if_t<I == std::tuple_size<Tuple>::value - 1> *unused = nullptr,
    typename ...Args>
constexpr auto for_each(Args &&...args)
-> std::enable_if_t<is_void<decltype(Functor<I, Tuple>::run(std::forward<Args>(args)...))>(),
                    decltype(Operation::run())> {
  Functor<I, Tuple>::run(std::forward<Args>(args)...);
  return Operation::run();
}

template<
    typename Tuple,
    template<size_t, typename> class Functor,
    typename Operation = for_each_operations::Nothing,
    size_t I = 0,
    std::enable_if_t<I == std::tuple_size<Tuple>::value - 1> *unused = nullptr,
    typename ...Args>
constexpr auto for_each(Args &&...args)
-> decltype(Operation::run(Functor<I, Tuple>::run(std::forward<Args>(args)...))) {
  return Operation::run(Functor<I, Tuple>::run(std::forward<Args>(args)...));
}

// On all other runs, we combine the results
// If both of the entries are voids...
template<
    typename Tuple,
    template<size_t, typename> class Functor,
    typename Operation = for_each_operations::Nothing,
    size_t I = 0,
    std::enable_if_t<I < std::tuple_size<Tuple>::value - 1> *unused = nullptr,
    typename ...Args>
constexpr auto for_each(Args &&...args)
-> std::enable_if_t<
    is_void<decltype(Functor<I, Tuple>::run(std::forward<Args>(args)...))>() &&
        is_void<decltype(for_each<Tuple,
                                  Functor,
                                  Operation,
                                  std::tuple_size<Tuple>::value - 1>(std::forward<Args>(args)...))>(),
    decltype(Operation::run())> {
  Functor<I, Tuple>::run(std::forward<Args>(args)...);
  for_each<Tuple, Functor, Operation, I + 1>(std::forward<Args>(args)...);
  return Operation::run();
}

template<
    typename Tuple,
    template<size_t, typename> class Functor,
    typename Operation = for_each_operations::Nothing,
    size_t I = 0,
    std::enable_if_t<I < std::tuple_size<Tuple>::value - 1> *unused = nullptr,
    typename ...Args>
constexpr auto for_each(Args &&...args)
-> std::enable_if_t<
    is_void<decltype(Functor<I, Tuple>::run(std::forward<Args>(args)...))>() &&
        !is_void<decltype(for_each<Tuple,
                                   Functor,
                                   Operation,
                                   std::tuple_size<Tuple>::value - 1>(std::forward<Args>(args)...))>(),
    decltype(Operation::run(for_each<Tuple, Functor, Operation, I + 1>(std::forward<Args>(args)...)))> {
  Functor<I, Tuple>::run(std::forward<Args>(args)...);
  return Operation::run(for_each<Tuple, Functor, Operation, I + 1>(std::forward<Args>(args)...));
}

template<
    typename Tuple,
    template<size_t, typename> class Functor,
    typename Operation = for_each_operations::Nothing,
    size_t I = 0,
    std::enable_if_t<I < std::tuple_size<Tuple>::value - 1> *unused = nullptr,
    typename ...Args>
constexpr auto for_each(Args &&...args)
-> std::enable_if_t<
    !is_void<decltype(Functor<I, Tuple>::run(std::forward<Args>(args)...))>() &&
        is_void<decltype(for_each<Tuple,
                                  Functor,
                                  Operation,
                                  std::tuple_size<Tuple>::value - 1>(std::forward<Args>(args)...))>(),
    decltype(Operation::run(Functor<I, Tuple>::run(std::forward<Args>(args)...)))> {
  auto &&tmp = Functor<I, Tuple>::run(std::forward<Args>(args)...);
  for_each<Tuple, Functor, Operation, I + 1>(std::forward<Args>(args)...);
  return Operation::run(tmp);
}

template<
    typename Tuple,
    template<size_t, typename> class Functor,
    typename Operation = for_each_operations::Nothing,
    size_t I = 0,
    std::enable_if_t<I + 1 < std::tuple_size<Tuple>::value> *unused = nullptr,
    typename ...Args>
constexpr auto for_each(Args &&...args)
-> std::enable_if_t<
    !is_void<decltype(Functor<I, Tuple>::run(std::forward<Args>(args)...))>() &&
        !is_void<decltype(for_each<Tuple,
                                   Functor,
                                   Operation,
                                   std::tuple_size<Tuple>::value - 1>(std::forward<Args>(args)...))>(),
    decltype(Operation::run(Functor<I, Tuple>::run(std::forward<Args>(args)...)))> {
  auto &&tmp = Functor<I, Tuple>::run(std::forward<Args>(args)...);
  return Operation::run(tmp, for_each<Tuple, Functor, Operation, I + 1>(std::forward<Args>(args)...));
}

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_FOR_EACH_HPP */
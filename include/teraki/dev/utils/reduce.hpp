#ifndef TERAKI_DEV_UTILS_REDUCE_HPP
#define TERAKI_DEV_UTILS_REDUCE_HPP

#include <tuple>

#include "teraki/dev/utils/unused.hpp"

namespace teraki {
namespace dev {

template<typename Tuple, typename Function, typename ReducedValue, size_t I = 0>
inline std::enable_if_t<I == std::tuple_size<Tuple>::value, ReducedValue>
reduce(const Tuple &tuple, const Function &function, const ReducedValue &reduced_value) {
  unused::sink(tuple);
  unused::sink(function);
  return reduced_value;
}

template<typename Tuple, typename Function, typename ReducedValue, size_t I = 0>
inline std::enable_if_t<I < std::tuple_size<Tuple>::value, ReducedValue>
reduce(Tuple &tuple, const Function function, const ReducedValue &reduced_value) {
  const ReducedValue updated_reduced_value = function(std::get<I>(tuple), reduced_value);
  return reduce<Tuple, Function, ReducedValue, I + 1>(tuple, function, updated_reduced_value);
}

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_REDUCE_HPP */
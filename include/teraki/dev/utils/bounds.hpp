#ifndef TERAKI_DEV_UTILS_BOUNDS_HPP
#define TERAKI_DEV_UTILS_BOUNDS_HPP

#include <algorithm>
#include <tuple>

#include "teraki/dev/utils/for_each.hpp"
#include "teraki/dev/utils/is_valid.hpp"

namespace teraki {
namespace dev {

struct Bounds {

 private:

  template<typename PC, typename PointType>
  static constexpr void static_checks() {
    static_assert(teraki::dev::is_valid_point_cloud<PC>(), "PointCloud is not valid.");
    static_assert(teraki::dev::is_valid_point<PointType>(), "PointType is not valid.");
    static_assert(std::is_same<typename std::decay<PointType>::type,
                               typename std::decay<typename PC::value_type>::type>::value,
                  "Your PointCloud's point type must be the same as the other PointTypes");
  }

 public:

  template<typename PC, typename PointType>
  static void range(PC &point_cloud, PointType &ranges) {

    static_checks<PC, PointType>();
    if (point_cloud.size() > 0) {
      teraki::dev::for_each<PointType, RangeComputer>(point_cloud, ranges);
    }
  }

  template<typename PC>
  static typename PC::value_type range(PC &point_cloud) {

    using PointType = typename PC::value_type;
    PointType ranges;
    range(point_cloud, ranges);
    return ranges;
  }

 private:

  template<size_t I, typename>
  struct RangeComputer {

    template<typename PC, typename PointType>
    static constexpr void run(PC &point_cloud, PointType &ranges) {
      const auto minmax = std::minmax_element(std::begin(point_cloud),
                                              std::end(point_cloud),
                                              [](const auto &p1, const auto &p2) {
                                                return std::get<I>(p1) < std::get<I>(p2);
                                              });
      std::get<I>(ranges) = std::get<I>(*minmax.second) - std::get<I>(*minmax.first);
    }
  };

 public:

  template<typename PC, typename PointType>
  static void min_and_max(const PC &point_cloud, PointType &mins, PointType &maxs) {

    static_checks<PC, PointType>();
    if (!point_cloud.empty()) {
      teraki::dev::for_each<PointType, MinAndMaxComputer>(point_cloud, mins, maxs);
    }
  }

  template<typename PC>
  static std::pair<typename PC::value_type, typename PC::value_type>
  min_and_range(const PC &point_cloud) {

    using PointType = typename PC::value_type;
    PointType mins, ranges;
    min_and_range(point_cloud, mins, ranges);
    return std::make_pair(mins, ranges);
  }

 private:

  template<size_t I, typename>
  struct MinAndMaxComputer {

    template<typename PC, typename PointType>
    static constexpr void run(PC &point_cloud, PointType &mins, PointType &maxs) {
      const auto minmax = std::minmax_element(std::begin(point_cloud),
                                              std::end(point_cloud),
                                              [](const auto &p1, const auto &p2) {
                                                return std::get<I>(p1) < std::get<I>(p2);
                                              });
      std::get<I>(mins) = std::get<I>(*minmax.first);
      std::get<I>(maxs) = std::get<I>(*minmax.second);
    }
  };
};

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_BOUNDS_HPP */
#ifndef TERAKI_DEV_UTILS_TUPLE_LOGICAL_EXPRESSIONS_HPP
#define TERAKI_DEV_UTILS_TUPLE_LOGICAL_EXPRESSIONS_HPP

#include <tuple>

#include "teraki/dev/utils/unused.hpp"

namespace teraki {
namespace dev {

// Returns true if the condition is true for all elements
template<
    typename Tuple,
    template<size_t, typename> class Condition,
    size_t I = 0,
    std::enable_if_t<I >= std::tuple_size<Tuple>::value> *unused = nullptr,
    typename ...Args>
constexpr bool all(Args &&...args) {
  unused::sink(std::forward<Args>(args)...);
  return true;
}

// Returns true if the condition is true for all elements
template<
    typename Tuple,
    template<size_t, typename> class Condition,
    size_t I = 0,
    std::enable_if_t<I < std::tuple_size<Tuple>::value> *unused = nullptr,
    typename ...Args>
constexpr bool all(Args &&...args) {
  if (Condition<I, Tuple>::run(std::forward<Args>(args)...)) {
    return all<Tuple, Condition, I + 1>(std::forward<Args>(args)...);
  }
  return false;
}

// Returns true if the condition is true for any element
template<
    typename Tuple,
    template<size_t, typename> class Condition,
    size_t I = 0,
    std::enable_if_t<I >= std::tuple_size<Tuple>::value> *unused = nullptr,
    typename ...Args>
constexpr bool any(Args &&...args) {
  unused::sink(std::forward<Args>(args)...);
  return false;
}

// Returns true if the condition is true for any element
template<
    typename Tuple,
    template<size_t, typename> class Condition,
    size_t I = 0,
    std::enable_if_t<I < std::tuple_size<Tuple>::value> *unused = nullptr,
    typename ...Args>
constexpr bool any(Args &&...args) {
  if (Condition<I, Tuple>::run(std::forward<Args>(args)...)) {
    return true;
  }
  return any<Tuple, Condition, I + 1>(std::forward<Args>(args)...);
}

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_TUPLE_LOGICAL_EXPRESSIONS_HPP */
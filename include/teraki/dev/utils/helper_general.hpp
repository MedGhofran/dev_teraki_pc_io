//
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <assert.h>

//
#include <math.h> 

// generic test
void test() {
  std::cout<<"test"<<std::endl;
}

// command line arguments
int usage(int argc, char* argv[], const std::vector<std::vector<std::string>>& vec_vec_argument, bool debug)
{
  // printing the arguments passed
  // note how the first argument is always the name of the file by default
  // so if you pass 2 arguments, argc is 3
  if (debug)
    {
      for(int i=0; i!=argc; i++)
	{
	  std::cout<<"arg["<<i<<"]="<<argv[i]<<std::endl;
	}
    }

  // this way you check if the user provided the right number of arguments
  // if not, abort the code in a controlled way using return -1
  // which tells you at what line the code crashed
  // the user can safely call your code with no argument
  // then it will crash but tell him first an example of how to use the code
  // let's say we want two arguments

  // number of arguments the user passes
  int N=0;
  if(vec_vec_argument.size()==0) {
    std::cout<<"You provided no examples in the usage function, so we are not sure how to check them."
	     <<" Even if no arguments, then still pass an empty vector. Will ABORT!!!"
	     <<std::endl;
    return -1;
  } else {
    N=vec_vec_argument[0].size(); 
  }

  // argc should should be the number of desired command line arguments plus 1
  if(argc!=(N+1))
    {
      std::cout<<"argc="<<argc<<" N="<<N<<std::endl;
      std::cout<<"You ran: ";
      for(int i=0; i!=argc; i++)
	{
	  std::cout<<" "<<argv[i];
	}
      std::cout<<std::endl;      
      std::cout<<"You need "<<N<<" arguments, but you passed "<<argc-1<<" arguments. Copy an example from below. Will ABORT!!!"<<std::endl;
      for (const auto& vec_argument : vec_vec_argument) {
	std::cout<<"Usage/Example:"
		 <<" "<<argv[0];
	for (const auto& argument : vec_argument) {
	  std::cout<<" "<<argument;
	}
	std::cout<<std::endl;
      }
      return -1;
    }//end if argc
  
  return 1;
} //end usage

// check if substring exists in string
bool containsSubString(const std::string& mainString, const std::string& subString, bool debug) {
  bool result=(mainString.find(subString) != std::string::npos);
  if (debug) {
    std::cout<<"mainString="<<mainString<<" subString="<<subString<<" contained="<<result<<std::endl;
  }
  return result;  
}

// erase first occurrence of given  substring from main string.
// https://thispointer.com/how-to-remove-substrings-from-a-string-in-c/
void eraseSubStr(std::string & mainString, const std::string & subString, bool debug)
{
  // Search for the substring in string
  size_t pos = mainString.find(subString);
  if (debug) {
    std::cout<<"mainString="<<mainString<<" subString="<<subString<<" pos="<<pos<<std::endl;
  }
  if (pos != std::string::npos)
    {
      if (debug) {
	std::cout<<"subString was found, so erasing it"<<std::endl;
      }
      mainString.erase(pos, subString.length());
      if (debug) {
	std::cout<<"After erasing mainString="<<mainString<<std::endl;
      }
    }
} // done function

// find if string ends in substring
bool endsWith(const std::string& mainString, const std::string& subString, bool debug)
{
  bool result=false;
  if (subString.size() <= mainString.size()) {
    result=std::equal(subString.rbegin(), subString.rend(), mainString.rbegin());
  }
  if (debug) {
    std::cout<<"mainString="<<mainString<<" subString="<<subString<<" endsWidth="<<result<<std::endl;
  }
  return result;
} // done function

// get extension of a file
std::string getExtension(const std::string& fileName, bool debug){
  std::string::size_type idx;
  idx=fileName.rfind('.');
  // std::string stem;
  std::string extension;
  if (idx!=std::string::npos) {
    // stem=fileName.substr(0,idx);
    extension=fileName.substr(idx+1);
  } else {
    // No extension found
    std::cout<<"No extension found in the fileName="<<fileName<<". Will ABORT!!!"<<std::endl;
    assert(false);
  }
  if (debug) {
    // std::cout<<"stem="<<stem<<std::endl;
    std::cout<<"extension="<<extension<<std::endl;
  }
  return extension;
}

// split a string by a delimiter
void split_string_by_delimiter(const std::string& original, const std::string& delimiter, std::vector<std::string>& vec_string, bool debug) {
  std::string s=original;
  size_t pos=0;
  std::string token;
  while ((pos=s.find(delimiter))!=std::string::npos) {
    // it means we found the delimiter in the current version of the string
    // retrieve the substring from beginning up to that token
    token=s.substr(0,pos);
    // append it to our vector
    vec_string.push_back(token);
    // remove both the token and the delimiter, so that we can then start again
    s.erase(0,pos+delimiter.length());
  }
  // at the end we need to add the one remaining
  vec_string.push_back(s);
  if (debug) {
    std::cout<<"Splitted string="<<s<<" by delimiter=\""<<delimiter<<"\" obtaining the vec_string:"<<std::endl;
    for (const auto& s: vec_string) {
      std::cout<<s<<std::endl;
    }
  }  
}

void convert_vec_string_to_vec_float(std::vector<std::string>& vec_string, std::vector<float>& vec_float, bool debug) {
  if (debug) {
    std::cout<<"vec_string has "<<vec_string.size()<<" elements."<<std::endl;
  }
  for (const auto& s : vec_string) {
    if (debug) {
      std::cout<<"s="<<s<<std::endl;
    }
    vec_float.push_back(std::atof(s.c_str()));
  }
}

template <typename T> 
void print_vec(std::vector<T>& vec, const std::string& vec_name, const std::string& el_name, bool debug) {
  if (debug==false) return;
  std::cout<<vec_name<<" has "<<vec.size()<<" elements:"<<std::endl;
  for (const auto& el : vec) {
    std::cout<<el_name<<"="<<el<<std::endl;
  }
}

// write strings to file
int writeStringCloudCompressedToBinaryFile(std::string& string_cloudCompressed, const std::string& fileName, bool debug) {
  // write the compressed string to a file
  if (debug) std::cout<<"Start writing compressed string to the output: "<<fileName<<std::endl;
  std::ofstream file;
  file.open(fileName,std::ios::binary);
  file<<string_cloudCompressed;
  if (false) {
    std::cout<<"string_cloudCompressed:"<<std::endl;
    std::cout<<string_cloudCompressed<<std::endl;
  }
  return 0;
} // done function

// read from a binary file a stringstream representing a compressed point cloud
int readStringCloudCompressedFromBinaryFile(std::string& string_cloudCompressed, const std::string& fileName, bool debug) {
  if (debug) std::cout<<"Start read input file: "<<fileName<<std::endl;
  if (debug) std::cout<<"Open file"<<std::endl;
  std::ifstream file(fileName,std::ios::binary);
  std::stringstream stringstream_cloudCompressed;
  stringstream_cloudCompressed<<file.rdbuf();
  string_cloudCompressed=stringstream_cloudCompressed.str();
  return 0;
} // done function

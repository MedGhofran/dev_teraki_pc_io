#ifndef TERAKI_DEV_UTILS_APPEND_INDICES_HPP
#define TERAKI_DEV_UTILS_APPEND_INDICES_HPP

#include <tuple>

#include "teraki/dev/utils/for_each.hpp"

namespace teraki {
namespace dev {

template<typename IndexType = size_t, typename OriginalPointType, template<typename, typename...> class PCContainer>
constexpr auto append_indices(const PCContainer<OriginalPointType> &original_pc) {

  using PointType = decltype(std::tuple_cat(std::declval<OriginalPointType>(), std::make_tuple(IndexType{})));
  PCContainer<PointType> pc(original_pc.size());
  auto point = begin(pc);
  IndexType i = 0;
  for (const auto &original_point : original_pc) {
    teraki::dev::for_each_lambda_enumerate(original_point,
                                           [&point](const auto &val, auto index) {
                                             std::get<index.value>(*point) = val;
                                           });
    std::get<std::tuple_size<OriginalPointType>::value>(*point) = i++;
    std::advance(point, 1);
  }
  return pc;
}

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_UTILS_APPEND_INDICES_HPP */

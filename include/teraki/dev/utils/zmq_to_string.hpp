#ifndef TERAKI_DEV_UTILS_ZMQ_TO_STRING_HPP
#define TERAKI_DEV_UTILS_ZMQ_TO_STRING_HPP

#include <zmq.hpp>

namespace std {
inline std::string to_string(zmq::message_t &message) {
  return std::string(static_cast<char *>(message.data()), message.size());
}
}

#endif /* TERAKI_DEV_UTILS_ZMQ_TO_STRING_HPP */

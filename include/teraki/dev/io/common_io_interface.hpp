#pragma once

#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>

#include "teraki/dev/utils/for_each.hpp"

namespace teraki {
namespace dev {

template<typename format>
struct common_io_interface {

  template<typename A, typename B>
  static auto not_equal(const A &a, const B &b) {
    return b.compare(a);
  }

  template<typename A, typename B>
  static auto equal(const A &a, const B &b) {
    return !not_equal(a, b);
  }

  template<typename S>
  static S &to_lower(S &s) {
    std::transform(begin(s), end(s), begin(s), ::tolower);
    return s;
  }

  /**
   * Parse a keyword and then a single element out of the specified string.
   * A return value of true denotes success.
   */
  template<typename Out>
  static auto parse_single(const std::string &string, Out &out) {
    std::stringstream sstream(string);
    std::string keyword;
    sstream >> keyword >> out;
    return !sstream.fail();
  }

  /**
   * Parse a keyword and then an iterable container out of the specified string.
   * A return value of true denotes success.
   */
  template<typename Out>
  static auto parse(const std::string &string, Out &out) {
    std::stringstream sstream(string);
    std::string keyword;
    sstream >> keyword;
    teraki::dev::for_each_lambda(out, [&sstream](auto &elem) { sstream >> elem; });
    return !sstream.fail();
  }

  /** Write the point cloud to a string. */
  template<bool ascii, typename PC, typename Names>
  static std::string to_string(const PC &point_cloud, const Names &names) {
    std::ostringstream ss;
    format::template to_stream<ascii>(ss, point_cloud, names);
    return ss.str();
  }

  /** Write the point cloud to the specified file */
  template<bool ascii, typename PC, typename Names>
  static void to_file(const std::string &filename, const PC &point_cloud, const Names &names) {
    if (ascii) {
      std::ofstream ofstream(filename);
      format::template to_stream<ascii>(ofstream, point_cloud, names);
    } else {
      std::ofstream ofstream(filename, std::ios::binary);
      format::template to_stream<ascii>(ofstream, point_cloud, names);
    }
  }

  /**
   * Read the point cloud from the specified string.
   * A return value of true denotes success.
   */
  template<typename PC>
  static bool from_string(const std::string &string, PC &point_cloud) {
    std::istringstream sstream(string);
    return format::from_stream(sstream, point_cloud);
  }

  /**
   * Read the point cloud from the specified file
   * A return value of true denotes success.
   */
  template<typename PC>
  static bool from_file(const std::string &filename, PC &point_cloud) {
    std::ifstream ifstream(filename);
    return format::from_stream(ifstream, point_cloud);
  }
};

} // namespace dev
} // namespace teraki
#ifndef TERAKI_DEV_FACTORIES_PLY_HPP
#define TERAKI_DEV_FACTORIES_PLY_HPP

#include <array>
#include <iostream> // For cerr
#include <istream>
#include <sstream>
#include <string>

#include "teraki/dev/io/raw.hpp"
#include "teraki/dev/io/common_io_interface.hpp"

#include "teraki/dev/utils/data_type.hpp"
#include "teraki/dev/utils/endian.hpp"
#include "teraki/dev/utils/endl.hpp"
#include "teraki/dev/utils/for_each.hpp"
#include "teraki/dev/utils/is_valid.hpp"
#include "teraki/dev/utils/print.hpp"

namespace teraki {
namespace dev {

struct ply : public common_io_interface<ply> {

 private:

  template<size_t I, typename PointType>
  struct PlyHeader {

    template<typename Names>
    constexpr static void run(std::ostream &ostream, const Names &names) {
      using T = std::tuple_element_t<I, PointType>;
      ostream << "property ";
      ostream << teraki::dev::data_type_name<T>();
      ostream << " ";
      ostream << std::get<I>(names);
      ostream << endl;
    }
  };

  template<size_t I, typename Tuple>
  struct TypeChecker {

    template<typename DataTypes>
    static constexpr void run(const DataTypes &data_types, bool &same) {
      // TODO(matt): This should be able to handle promotion
      same &= std::get<I>(data_types) == teraki::dev::data_type<std::tuple_element_t<I, Tuple>>();
    }
  };

 public:

  /** Write the point cloud to the specified output stream */
  template<bool ascii, typename PC, typename Names>
  static void to_stream(std::ostream &ostream, const PC &point_cloud, const Names &names) {

    static_assert(teraki::dev::is_valid_point_cloud<PC>(),
                  "Your PointCloud is not valid. It must be of type teraki::dev::PointCloud.");

    // Print the header
    ostream << "ply" << endl;
    ostream << "format ";
    if (ascii) {
      ostream << "ascii";
    } else {
      ostream << (teraki::dev::Endian::little ? "binary_little_endian" : "binary_big_endian");
    }
    ostream << " 1.0" << endl;
    ostream << "element vertex " << point_cloud.size() << endl;
    teraki::dev::for_each<typename PC::value_type, PlyHeader>(ostream, names);
    ostream << "end_header" << endl;

    // Print the points
    teraki::dev::raw::to_stream<ascii>(ostream, point_cloud);
  }

  /** Read the point cloud from the specified input stream */
  template<typename PC>
  static bool from_stream(std::istream &ply, PC &point_cloud) {

    static_assert(teraki::dev::is_valid_point_cloud<PC>(),
                  "Your PointCloud is not valid. It must be of type teraki::dev::PointCloud.");
    using Point = typename PC::value_type;

    auto next_keyword = [](auto &ply, auto &line) {
      std::string keyword;
      while (std::getline(ply, line)) {
        if (!line.empty()) {
          std::istringstream ss(line);
          ss >> keyword;
          if (not_equal("comment", keyword)) {
            to_lower(keyword);
            break;
          }
        }
      }
      return keyword;
    };

    // Make sure that the file starts with "ply"
    std::string line;
    if (not_equal("ply", next_keyword(ply, line))) {
      std::cerr << "ply file doesn't start with 'ply'";
      return false;
    }

    // Read the header until we find "end_header"
    bool end_header_specified = false;
    bool num_points_specified = false;
    bool format_specified = false;
    bool valid_keyword = false;
    bool ascii = true;
    size_t num_points = 0;
    size_t dimensions = 0;
    constexpr size_t point_cloud_dimensions = std::tuple_size<Point>::value;
    std::array<decltype(teraki::dev::data_type<int>()), point_cloud_dimensions> data_types{};
    std::string keyword;
    while (true) {

      // Get the next keyword
      if (!valid_keyword) {
        keyword = next_keyword(ply, line);
      } else {
        // Invalidate the keyword after this
        valid_keyword = false;
      }

      if (equal("end_header", keyword)) {

        end_header_specified = true;
        break;

      } else if (equal("format", keyword)) {

        if (format_specified) {
          std::cerr << "format was specified twice";
          return false;
        }
        format_specified = true;

        std::string format_type;
        std::istringstream format_ss(line);
        format_ss >> keyword >> format_type;
        if (not_equal("ascii", format_type)) {
          ascii = false;
        }

      } else if (equal("element", keyword)) {

        std::string element_type;
        size_t num_elements = 0;
        std::istringstream element_ss(line);
        element_ss >> keyword >> element_type >> num_elements;

        if (equal("vertex", element_type)) {

          if (num_points_specified) {
            std::cerr << "number of points was specified twice";
            return false;
          }
          num_points_specified = true;
          num_points = num_elements;

          // Now we need to read off the element properties
          while (true) {

            // Get the next keyword
            keyword = next_keyword(ply, line);
            if (not_equal("property", keyword)) {
              valid_keyword = true;
              break;
            }
            if (dimensions >= point_cloud_dimensions) {
              // The point type apparently
              continue;
            }
            std::string property_type;
            std::istringstream property_ss(line);
            property_ss >> keyword >> property_type;
            data_types.at(dimensions++) = teraki::dev::data_type(property_type);
          }
        }

      } else if (keyword.empty()) {

        return false;

      } else {
        // Some unknown keyword or some property in a non-vertex block. Ignore it.
      }
    }  // read header

    // If you got here, but the inHeader flag is still set, that means you
    // reached the end of the file before reading "end_header". This is an invalid file.
    if (!end_header_specified) {
      std::cerr << "No end_header specified";
      return false;
    }

    // Make sure that the format was specified
    if (!format_specified) {
      std::cerr << "A format (binary or ascii) was not specified";
      return false;
    }

    // Make sure that you specified the number of points
    if (!num_points_specified) {
      std::cerr << "The number of points was not specified in the header.";
      return false;
    }

    // If there are less properties than the point cloud, then we can't fill the point cloud
    if (dimensions < point_cloud_dimensions) {
      std::cerr << "The ply file doesn't seem to contain enough dimensions to fill this point cloud";
      return false;
    }

    // We need to check that the data types in the ply file match the PointCloud that we are trying to fill
    bool same_types = true;
    teraki::dev::for_each<Point, TypeChecker>(data_types, same_types);
    if (!same_types) {
      std::cerr << "The ply file does not have the same types as the point cloud you are trying to fill";
      return false;
    }

    // For each line in the body of the ply file
    point_cloud.resize(num_points);
    if (ascii) {
      return teraki::dev::raw::from_stream<true>(ply, point_cloud);
    }
    return teraki::dev::raw::from_stream<false>(ply, point_cloud);
  }
};

} // namespace dev
} // namespace teraki

#endif /* TERAKI_DEV_FACTORIES_PLY_HPP */
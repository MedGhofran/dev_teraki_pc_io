#pragma once

#include <algorithm>
#include <array>
#include <iostream> // For cerr
#include <istream>
#include <sstream>
#include <string>
#include <array>
#include <vector>
#include <cmath>
#include <cstring>

#include "teraki/dev/io/common_io_interface.hpp"
#include "teraki/dev/io/raw.hpp"

#include "teraki/dev/utils/data_type.hpp"
#include "teraki/dev/utils/endian.hpp"
#include "teraki/dev/utils/for_each.hpp"
#include "teraki/dev/utils/is_valid.hpp"
#include "teraki/dev/utils/print.hpp"

namespace teraki {
namespace dev {

struct numpy : common_io_interface<numpy> {

 private:

  template<size_t I, typename Point>
  struct WriteNamesAndTypes {

    template<typename NamesIterator>
    static void run(std::ostream &ostream, NamesIterator &names_iterator, bool &success) {
      using T = std::tuple_element_t<I, Point>;
      ostream << "('" << *names_iterator << "', '" << (teraki::dev::Endian::little ? "<" : ">");
      if (std::is_floating_point<T>::value) {
        ostream << "f";
      } else if (std::is_integral<T>::value) {
        if (std::is_unsigned<T>::value) {
          ostream << "u";
        } else {
          ostream << "i";
        }
      } else {
        success = false;
        return;
      }
      ostream << sizeof(T);
      ostream << "')";
      if (I < std::tuple_size<Point>::value - 1) {
        ostream << ", ";
      }
      std::advance(names_iterator, 1);
    }
  };

  template<size_t I, typename Point>
  struct TypeChecker {

    template<typename T, typename NumpyTypeString>
    static std::string errorMessage(const NumpyTypeString &numpy_type) {
      std::ostringstream ss;
      ss << "The numpy file says that the " << I << "th entry of the point cloud should be of type " << numpy_type
         << ", but that entry of your point cloud is a " << teraki::dev::data_type_name<T>();
      return ss.str();
    }

    template<typename Types>
    static void run(const Types &types, std::string &error_message) {
      using T = std::tuple_element_t<I, Point>;
      const auto &type = std::get<I>(types);

      // First check whether the type (float, unsigned, or integral) is correct
      const std::string type_string(1, type.at(0));
      bool correct_types = true;
      if (std::is_floating_point<T>::value) {
        correct_types &= equal("f", type_string);
      } else if (std::is_integral<T>::value) {
        if (std::is_unsigned<T>::value) {
          correct_types &= equal("u", type_string);
        } else {
          correct_types &= equal("i", type_string);
        }
      } else {
        correct_types = false;
      }

      // Now check the size of that type
      const std::string size_string(1, type.at(1));
      std::istringstream ss(size_string);
      size_t size;
      ss >> size;
      correct_types &= size == sizeof(T);

      if (!correct_types) {
        error_message = errorMessage<T>(type);
      }
    }
  };

  // This version will compile if the string type has a size method (like std::string)
  template<typename String>
  constexpr static auto string_length(const String &s) -> decltype(s.size()) {
    return s.size();
  }

  // This version will compile if the string type works with strlen.
  // Note that this SFINAE will break if the string works with both string_length methods
  template<typename String>
  constexpr static auto string_length(const String &s) -> decltype(strlen(s)) {
    return strlen(s);
  }

 public:

  /** Write the point cloud to the specified output stream */
  template<bool ascii, typename PC, typename Names, std::enable_if_t<ascii> *unnused = nullptr>
  static bool to_stream(std::ostream &, const PC &, const Names &) {
    static_assert(!ascii, "numpy can only work with binary files.");
    return false;
  }

  /** Write the point cloud to the specified output stream */
  template<bool ascii, typename PC, typename Names, std::enable_if_t<!ascii> *unnused = nullptr>
  static bool to_stream(std::ostream &ostream, const PC &point_cloud, const Names &names) {

    static_assert(teraki::dev::is_valid_point_cloud<PC>(), "Your PointCloud is not valid.");
    using Point = typename PC::value_type;
    static constexpr auto point_size = std::tuple_size<Point>::value;

    ostream << static_cast<char>(0x93);
    ostream << "NUMPY";
    ostream.put((char) 1);
    ostream.put((char) 0);

    // Now we need to calculate how long the rest of the header will be
    std::string pc_size_string = std::to_string(point_cloud.size());
    uint16_t header_length = 53 + pc_size_string.size() + 13 * point_size - 1;
    size_t index = 0;
    for (const auto &name : names) {
      if (++index > point_size) {
        break;
      }
      header_length += numpy::string_length(name);
    }
    ostream.write(reinterpret_cast<char *>(&header_length), 2);
    ostream << "{'descr': ["; // 11
    auto name_iterator = cbegin(names);
    bool success = true;
    teraki::dev::for_each<Point, WriteNamesAndTypes>(ostream, name_iterator, success);
    if (!success) {
      return false;
    }
    ostream << "], "; // 3
    ostream << "'fortran_order': False, "; // 24
    ostream << "'shape': (" << pc_size_string << ",), }"; // 15 + pc_size_string
    ostream << "\n";

    // Print the points
    teraki::dev::raw::to_stream<ascii>(ostream, point_cloud);
    return true;
  }

  /**
   * Read the point cloud from the specified input stream.
   * A returned value of true denotes success.
   */
  template<typename PC>
  static bool from_stream(std::istream &istream, PC &point_cloud) {

    static_assert(teraki::dev::is_valid_point_cloud<PC>(), "Your PointCloud is not valid.");
    using Point = typename PC::value_type;
    static constexpr auto point_size = std::tuple_size<Point>::value;
    std::array<std::string, point_size> types;

    // A function for removing whitespace and then converting to lowercase
    auto clean = [](auto &str) -> decltype(auto) {
      str.erase(remove(str.begin(), str.end(), ' '), str.end());
      to_lower(str);
      return str;
    };

    char x93;
    istream.read(&x93, 1);
    if (x93 != (char) 0x93) {
      std::cerr << "numpy files must start with the 0x93 character";
      return false;
    }

    std::string numpy;
    numpy.resize(5);
    istream.read(&numpy[0], 5);
    if (not_equal("numpy", to_lower(numpy))) {
      std::cerr << numpy << "!=numpy" << std::endl;
      return false;
    }

    // Read the numpy data version (typically 1.0)0
    char major_version, minor_version;
    istream.read(&major_version, 1);
    istream.read(&minor_version, 1);

    // Read the header size
    uint16_t header_size;
    istream.read(reinterpret_cast<char *>(&header_size), 2);
    std::cout << "Header size : " << header_size << std::endl;

    // Read the rest of the header into a stringstream
    std::string header_string;
    header_string.resize(header_size);
    istream.read(&header_string[0], header_string.size());
    std::istringstream header(header_string);

    // Now we are going to read word for word from the header
    std::string token, boilerplate;
    size_t num_points = 1;
    bool valid_shape = false;
    bool valid_types = false;
    // While there is still a token in quotes...
    while (std::getline(header, boilerplate, '\'') && std::getline(header, token, '\'')) {
      clean(token);
      if (equal("descr", token)) {
        // Now let's grab everything in the square brackets
        std::getline(header, boilerplate, '[');
        std::getline(header, token, ']');
        std::istringstream type_ss(token);
        for (auto &type : types) {

          // The first quoted thing is the name. ignore it.
          // The second quoted thing is the type
          // This if statement just checks whether we can actually read the type
          if (!(std::getline(type_ss, boilerplate, '\'') &&
              std::getline(type_ss, token, '\'') &&
              std::getline(type_ss, boilerplate, '\'') &&
              std::getline(type_ss, type, '\''))) {
            // If we got here, then it means that the header does not have enough types to fill the point cloud.
            // This would happen if, for instance, you are trying to fill a 3D point cloud from a file that
            // has data for a 2D point cloud.
            return false;
          }

          // TODO(matt): For now we just disregard the endianness symbol:
          // https://docs.scipy.org/doc/numpy/reference/generated/numpy.dtype.byteorder.html#numpy.dtype.byteorder
          if (type.size() < 2) {
            return false;
          } else if (type.size() == 3) {
            // The first symbol indicates endianness.
            // Handle it, and then erase it
            type.erase(0, 1);
          }
        }
        valid_types = true;
      } else if (equal("fortran_order", token)) {
        std::getline(header, token, ':');
        std::getline(header, token, ',');
        // TODO(matt): currently we only handle the case where this is false
        if (not_equal("false", clean(token))) {
          std::cerr << "fortran_order must currently be false, not '" << token << "'\n";
          return false;
        }
      } else if (equal("shape", token)) {
        // We need to get the number of points from the shape.
        // To do this, we simply multiply all of the dimensions in the shape.
        // To get the dimensions, we look for everything in the parentheses
        std::getline(header, boilerplate, '(');
        std::getline(header, token, ')');
        // The token now contains all of the dimensions,
        // e.g. 100, OR 100,1,1 OR 5,3
        // So we will now look for all of the dimensions and multiply them together
        std::istringstream dims(token);
        while (std::getline(dims, token, ',')) {
          std::istringstream n_points(token);
          size_t dim;
          if (n_points >> dim) {
            num_points *= dim;
          }
        }
        valid_shape = true;
      } else {

        std::cerr << "unknown keyword in numpy header:" << token;
        return false;
      }
    }

    if (!valid_shape) {
      std::cerr << "We could not parse the shape" << std::endl;
      return false;
    }
    if (!valid_types) {
      std::cerr << "We could not parse the types" << std::endl;
      return false;
    }

    // Now we need to check whether the types in the file are correct.
    std::string error_message;
    teraki::dev::for_each<Point, TypeChecker>(types, error_message);
    if (!error_message.empty()) {
      std::cerr << error_message << std::endl;
      return false;
    }

    // Finally, read the points
    // TODO(matt): Actually, we need to be reading with skip_bits (or by passing a vector of the actual types in the point cloud)
    point_cloud.resize(num_points);
    return teraki::dev::raw::from_stream<false>(istream, point_cloud);
  }
};

} // namespace dev
} // namespace teraki

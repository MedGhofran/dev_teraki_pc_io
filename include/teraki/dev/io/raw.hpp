#pragma once

#include <ostream>
#include <istream>
#include <sstream>
#include <string>
#include <typeinfo>

#include "teraki/dev/io/common_io_interface.hpp"
#include "teraki/dev/utils/endl.hpp"
#include "teraki/dev/utils/for_each.hpp"
#include "teraki/dev/utils/is_valid.hpp"
#include "teraki/dev/utils/numerical_chars.hpp"
#include "teraki/dev/utils/print.hpp"
#include "teraki/dev/utils/unused.hpp"

namespace teraki {
namespace dev {

struct raw {

 public:

  /** Ascii write the point cloud to the specified output stream */
  template<bool ascii, typename PC, std::enable_if_t<ascii> *unused = nullptr>
  static void to_stream(std::ostream &ostream, const PC &point_cloud) {
    static_assert(teraki::dev::is_valid_point_cloud<PC>(), "Your PointCloud is not valid.");
    teraki::dev::print(point_cloud, ostream, " ", endl);
  }

  /** Binary write the point cloud to the specified output stream */
  template<bool ascii, typename PC, std::enable_if_t<!ascii> *unused = nullptr>
  static void to_stream(std::ostream &ostream, const PC &point_cloud) {
    static_assert(teraki::dev::is_valid_point_cloud<PC>(), "Your PointCloud is not valid.");
    for (auto &point : point_cloud) {
      teraki::dev::for_each_lambda(point, [&ostream](const auto &val) {
        ostream.write((const char *) &val, sizeof(decltype(val)));
      });
    }
  }

  /**
   * Ascii read the point cloud from the specified input stream.
   * A returned value of true denotes success.
   */
  template<bool ascii, typename PC, std::enable_if_t<ascii> *unused = nullptr>
  static bool from_stream(std::istream &istream, PC &point_cloud) {
    for (auto &point : point_cloud) {
      std::string line;
      std::getline(istream, line);
      std::stringstream ss(line);
      teraki::dev::for_each_lambda(point, [&ss](auto &val) {
        using numerical_chars::operator>>;
        ss >> val;
      });
      if (ss.fail()) {
        return false;
      }
    }
    return !istream.fail();
  }

  /**
 * Read the point cloud from the specified input stream.
 * A return value of true denotes success.
 */
  template<bool ascii, typename PC, std::enable_if_t<!ascii> *unused = nullptr>
  static bool from_stream(std::istream &istream, PC &point_cloud) {
    for (auto &point : point_cloud) {
      teraki::dev::for_each_lambda(point, [&istream](auto &val) {
        istream.read((char *) &val, sizeof(decltype(val)));
      });
    }
    return !istream.fail();
  }

  /**
  * Read the point cloud from the specified input stream.
  * A return value of true denotes success.
  */
  template<bool ascii, typename PC, std::enable_if_t<!ascii> *unused = nullptr>
  static bool from_stream(std::istream &istream, PC &point_cloud, size_t skip_bits) {
    for (auto &point : point_cloud) {
      teraki::dev::for_each_lambda(point, [&istream](auto &val) {
        istream.read((char *) &val, sizeof(decltype(val)));
      });
      istream.ignore(skip_bits);
    }
    return !istream.fail();
  }
};

} // namespace dev
} // namespace teraki

#pragma once

#include <algorithm>
#include <array>
#include <iostream> // For cerr
#include <istream>
#include <sstream>
#include <string>
#include <array>
#include <cmath>

#include "teraki/dev/io/common_io_interface.hpp"
#include "teraki/dev/io/raw.hpp"

#include "teraki/dev/utils/data_type.hpp"
#include "teraki/dev/utils/endl.hpp"
#include "teraki/dev/utils/for_each.hpp"
#include "teraki/dev/utils/is_valid.hpp"
#include "teraki/dev/utils/print.hpp"

namespace teraki {
namespace dev {

struct pcd : public common_io_interface<pcd> {

 private:

  template<size_t I, typename>
  struct Fields {
    template<typename Names>
    constexpr static void run(std::ostream &ostream, const Names &names) {
      ostream << " " << std::get<I>(names);
    }
  };

  template<size_t I, typename PointType>
  struct Sizes {
    constexpr static void run(std::ostream &ostream) {
      ostream << " " << sizeof(std::tuple_element_t<I, PointType>);
    }
  };

  template<size_t I, typename PointType>
  struct Types {
    constexpr static void run(std::ostream &ostream, bool &valid_types) {
      using T = std::tuple_element_t<I, PointType>;
      if (std::is_floating_point<T>::value) {
        ostream << " F";
      } else if (std::is_integral<T>::value) {
        if (std::is_unsigned<T>::value) {
          ostream << " U";
        } else {
          ostream << " I";
        }
      } else {
        ostream << " O"; // O denotes "other" although this is non-standard PCD
        valid_types = false;
      }
    }
  };

  template<size_t I, typename PointType>
  struct Counts {
    constexpr static void run(std::ostream &ostream) {
      ostream << " 1";
    }
  };

  template<size_t I, typename Point>
  struct SizeChecker {

    template<typename Sizes>
    static void run(const Sizes &sizes, bool &correct_sizes) {
      correct_sizes &= std::get<I>(sizes) <= sizeof(std::tuple_element_t<I, Point>);
    }
  };

  template<size_t I, typename Point>
  struct TypeChecker {

    template<typename Types>
    static void run(const Types &types, bool &correct_types) {
      using T = std::tuple_element_t<I, Point>;
      const auto &type = std::get<I>(types);
      if (std::is_floating_point<T>::value) {
        correct_types &= !not_equal("F", type);
      } else if (std::is_integral<T>::value) {
        if (std::is_unsigned<T>::value) {
          correct_types &= !not_equal("U", type);
        } else {
          correct_types &= !not_equal("I", type);
        }
      } else {
        correct_types = false;
      }
    }
  };

 public:

  /** Write the point cloud to the specified output stream */
  template<bool ascii, typename PC, typename Names>
  static bool to_stream(std::ostream &ostream, const PC &point_cloud, const Names &names) {

    static_assert(teraki::dev::is_valid_point_cloud<PC>(), "Your PointCloud is not valid.");

    // Print the header
    using Point = typename PC::value_type;
    ostream << "VERSION .7";
    ostream << endl << "FIELDS";
    teraki::dev::for_each<Point, Fields>(ostream, names);
    ostream << endl << "SIZE";
    teraki::dev::for_each<Point, Sizes>(ostream);
    ostream << endl << "TYPE";
    bool valid_types = true;
    teraki::dev::for_each<Point, Types>(ostream, valid_types);
    // TODO We should probably exit here and force the user to deal with the invalid types
    ostream << endl << "COUNT";
    teraki::dev::for_each<Point, Counts>(ostream);
    ostream << endl << "WIDTH " << point_cloud.size();
    ostream << endl << "HEIGHT 1";
    ostream << endl << "VIEWPOINT 0 0 0 1 0 0 0";
    ostream << endl << "POINTS " << point_cloud.size();
    ostream << endl << "DATA " << (ascii ? "ascii" : "binary") << endl;

    // Print the points
    teraki::dev::raw::to_stream<ascii>(ostream, point_cloud);
    return true;
  }

  /**
   * Read the point cloud from the specified input stream.
   * A returned value of true denotes success.
   */
  template<typename PC>
  static bool from_stream(std::istream &pcd, PC &point_cloud) {

    static_assert(teraki::dev::is_valid_point_cloud<PC>(), "Your PointCloud is not valid.");
    using Point = typename PC::value_type;
    static constexpr auto point_size = std::tuple_size<Point>::value;

    auto next_keyword = [](auto &pcd, auto &line) {
      std::string keyword;
      while (std::getline(pcd, line)) {
        if (!line.empty()) {
          std::istringstream ss(line);
          ss >> keyword;
          if (!keyword.empty() && keyword.at(0) != '#') {
            to_lower(keyword);
            break;
          }
        }
      }
      return keyword;
    };

    // Read the version
    std::string line;
    if (not_equal("version", next_keyword(pcd, line))) {
      return false;
    }
    {
      float version;
      if (!parse_single(line, version)) {
        return false;
      }
      if (std::abs(version - .7) > .01) {
        std::cerr << "We only support version .7 in pcd files";
        return false;
      }
    }

    // The next entries should be the field names, but we don't need them
    if (not_equal("fields", next_keyword(pcd, line))) {
      return false;
    }

    // Read the type sizes
    if (not_equal("size", next_keyword(pcd, line))) {
      return false;
    }
    {
      // Make sure that the sizes are less than or equal to what we have in the point cloud
      std::array<size_t, point_size> sizes;
      if (!parse(line, sizes)) {
        return false;
      }
      bool correct_sizes = true;
      teraki::dev::for_each<Point, SizeChecker>(sizes, correct_sizes);
      if (!correct_sizes) {
        return false;
      }
    }

    // Check the types (float, unsigned, or int)
    if (not_equal("type", next_keyword(pcd, line))) {
      return false;
    }
    {
      // Make sure that the types match what is in the point cloud.
      // For the time being, the types must strictly match.
      // However, in the future, we could relax this to let
      // uints be put into ints, and/or ints into floats.
      // In that case, you would need to check that you don't overflow.
      std::array<std::string, point_size> types;
      if (!parse(line, types)) {
        return false;
      }
      bool correct_types = true;
      teraki::dev::for_each<Point, TypeChecker>(types, correct_types);
      if (!correct_types) {
        return false;
      }
    }

    // Read the counts. We only support ones.
    if (not_equal("count", next_keyword(pcd, line))) {
      return false;
    }
    {
      std::array<size_t, point_size> counts;
      if (!parse(line, counts)) {
        return false;
      }
      for (const auto &count : counts) {
        if (count != 1) {
          return false;
        }
      }
    }

    // Read the width, height, and viewpoint. We ignore these
    if (not_equal("width", next_keyword(pcd, line))) {
      return false;
    }
    if (not_equal("height", next_keyword(pcd, line))) {
      return false;
    }
    if (not_equal("viewpoint", next_keyword(pcd, line))) {
      return false;
    }

    // Read the number of points
    if (not_equal("points", next_keyword(pcd, line))) {
      return false;
    }
    size_t num_points;
    if (!parse_single(line, num_points)) {
      return false;
    }

    // Determine whether this is a binary or ascii file
    if (not_equal("data", next_keyword(pcd, line))) {
      return false;
    }
    std::string format;
    if (!parse_single(line, format)) {
      return false;
    }

    // Finally, read the points
    point_cloud.resize(num_points);
    if (not_equal("binary", to_lower(format))) {
      // Ascii
      return teraki::dev::raw::from_stream<true>(pcd, point_cloud);
    } else {
      // Binary
      return teraki::dev::raw::from_stream<false>(pcd, point_cloud);
    }
  }
};

} // namespace dev
} // namespace teraki
